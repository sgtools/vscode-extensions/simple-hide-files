// Copyright (C) 2023 Sebastien Guerri
//
// simple-hide-files is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or any later version.
//
// simple-hide-files is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.

import * as vscode from "vscode";

import { Dependency } from "./dependency";


export async function get(): Promise<FolderHiddenFiles[]>
{
	const hiddenFiles: FolderHiddenFiles[] = [];

	vscode.workspace.workspaceFolders?.forEach(folder => {
		const folderHiddenFiles: FolderHiddenFile[] = [];

		const settings = vscode.workspace.getConfiguration("files", folder).inspect("exclude")?.workspaceFolderValue
		if (typeof settings !== "undefined") {
			const settingsJSON = JSON.parse(JSON.stringify(settings));
			Object.keys(settingsJSON).forEach(key => {
				folderHiddenFiles.push({
					path: key,
					hidden: settingsJSON[key]
				});
			});
		}

		hiddenFiles.push({
			folder: folder,
			hiddenFiles: folderHiddenFiles
		});
	});

	return hiddenFiles;
}


async function updateItem(folder: vscode.WorkspaceFolder, key: string, value: boolean | undefined): Promise<void>
{
	let excludedFiles = vscode.workspace.getConfiguration("files", folder).inspect("exclude")?.workspaceFolderValue;
	if (typeof excludedFiles === "undefined") {
		excludedFiles = {};
	}

	const excludedFilesJSON = JSON.parse(JSON.stringify(excludedFiles));

	excludedFilesJSON[key] = value;

	await vscode.workspace.getConfiguration("files", folder).update("exclude", excludedFilesJSON, vscode.ConfigurationTarget.WorkspaceFolder);
	await vscode.window.showInformationMessage("Settings updated");
}

async function updateItems(folder: vscode.WorkspaceFolder, items: [string, boolean | undefined][]): Promise<void>
{
	let excludedFiles = vscode.workspace.getConfiguration("files", folder).inspect("exclude")?.workspaceFolderValue;
	if (typeof excludedFiles === "undefined") {
		excludedFiles = {};
	}

	const excludedFilesJSON = JSON.parse(JSON.stringify(excludedFiles));

	items.forEach(([key, value]) => {
		excludedFilesJSON[key] = value;
	});

	await vscode.workspace.getConfiguration("files", folder).update("exclude", excludedFilesJSON, vscode.ConfigurationTarget.WorkspaceFolder);
	await vscode.window.showInformationMessage("Settings updated");
}


export async function update(element: Dependency): Promise<void>
{
	if (element.isFolder) return;
	await updateItem(element.folder, element.label, element.isHidden);
}

export async function updateName(element: Dependency, oldName: string)
{
	if (element.isFolder) return;
	await updateItems(element.folder, [
		[oldName, undefined],
		[element.label, element.isHidden]
	]);
}

export async function remove(element: Dependency)
{
	if (element.isFolder) return;
	await updateItem(element.folder, element.label, undefined);
}

export async function add(element: Dependency, key: string, value: boolean)
{
	if (!element.isFolder) return;
	await updateItem(element.folder, key, value);
}

