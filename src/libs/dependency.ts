// Copyright (C) 2023 Sebastien Guerri
//
// simple-hide-files is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or any later version.
//
// simple-hide-files is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.

import * as path from "path";
import * as vscode from "vscode";


export class Dependency extends vscode.TreeItem
{
	constructor(
		public label: string,
		public readonly isFolder: boolean,
		private hidden: boolean,
		public readonly folder: vscode.WorkspaceFolder,
		public hiddenFiles?: FolderHiddenFile[],
		public readonly parent?: Dependency
	) {
		super(label, isFolder ? vscode.TreeItemCollapsibleState.Expanded : undefined);
		this.iconPath = this.getIcon();
		this.contextValue = isFolder ? "folder" : "element";
	}

	public get isHidden(): boolean
	{
		return this.hidden;
	}

	public async updateStatus(): Promise<void>
	{
		this.hidden = !this.hidden;
		this.iconPath = this.getIcon();
	}

	public async updateName(newName: string): Promise<void>
	{
		this.label = newName;
	}

	private getIcon(): { light: string, dark: string }
	{
		if (this.isFolder) {
			return {
				light: path.join(__filename, "..", "..", "resources", "light", "folder.svg"),
				dark: path.join(__filename, "..", "..", "resources", "dark", "folder.svg")
			};
		} else {
			return {
				light: path.join(__filename, "..", "..", "resources", "light", `eye${this.hidden ? '-closed' : ''}.svg`),
				dark: path.join(__filename, "..", "..", "resources", "dark", `eye${this.hidden ? '-closed' : ''}.svg`)
			};
		}
	}
}