// Copyright (C) 2023 Sebastien Guerri
//
// simple-hide-files is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or any later version.
//
// simple-hide-files is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.

import * as vscode from "vscode";
import * as settings from "./libs/settings";

import { Dependency } from "./libs/dependency";

/**
 * Main provider class
 */
export class HideFilesProvider implements vscode.TreeDataProvider<Dependency>
{
	private _onDidChangeTreeData: vscode.EventEmitter<Dependency | undefined> = new vscode.EventEmitter<Dependency | undefined>();
	readonly onDidChangeTreeData: vscode.Event<Dependency | undefined> = this._onDidChangeTreeData.event;

	/**
	 * Folders
	 */
	private parents: Dependency[] = [];

	constructor()
	{
		vscode.workspace.onDidChangeConfiguration(async () => {
			await this.reload();
			await this.refresh();
		});

		vscode.workspace.onDidChangeWorkspaceFolders(async () => {
			await this.reload();
			await this.refresh();
		});

		this.reload();
	}

	/**
	 * Retrieve children for a given offset
	 * @param offset Offset
	 * @returns Indexes of children offsets
	 */
	async getChildren(element?: Dependency): Promise<Dependency[]>
	{
		if (vscode.workspace.workspaceFolders?.length === 1) {

			const elements: Dependency[] = [];
			const parent = this.parents[0];
			parent.hiddenFiles?.forEach(hiddenFile => {
				elements.push(new Dependency(
					hiddenFile.path,
					false,
					hiddenFile.hidden,
					parent.folder,
					undefined,
					parent
				));
			});

			return elements;

		} else {
			if (element) {
				const elements: Dependency[] = [];
				element.hiddenFiles?.forEach(hiddenFile => {
					elements.push(new Dependency(
						hiddenFile.path,
						false,
						hiddenFile.hidden,
						element.folder,
						undefined,
						element
					));
				});
				return elements;
			} else {
				this.parents = [];
				const data = await settings.get();
				data.forEach(item => {
					this.parents.push(new Dependency(
						item.folder.name,
						true,
						false,
						item.folder,
						item.hiddenFiles
					));
				});
				return this.parents;
			}
		}
	}

	/**
	 * Retrieve a specific view item
	 * @param offset Line to retrieve
	 * @returns vscode.TreeItem
	 */
	async getTreeItem(element: Dependency): Promise<vscode.TreeItem>
	{
		return element;
	}

	////// ACTIONS //////////////////////////////////////////////////////////////////////////////

	/**
	 * Refresh all items
	 * @param offset Line to refresh (optional)
	 */
	async refresh(element?: Dependency): Promise<void>
	{
		// if (typeof element === "undefined") {
		// 	this._onDidChangeTreeData.fire(undefined);
		// } else {
			this._onDidChangeTreeData.fire(element)
		// }
	}

	async reload(): Promise<void>
	{
		vscode.commands.executeCommand("setContext", "hideFiles.singleton", vscode.workspace.workspaceFolders?.length === 1);

		if (vscode.workspace.workspaceFolders?.length === 1) {
			this.parents = [];
			const item = (await settings.get())[0];
			const parent = new Dependency(
				item.folder.name,
				true,
				false,
				item.folder,
				item.hiddenFiles
			);
			this.parents.push(parent);
		}
	}

	async reloadAndRefresh(): Promise<void>
	{
		await this.reload();
		await this.refresh();
	}

	/**
	 * Update entry
	 * @param offset Line to update
	 */
	async updateItem(element: Dependency): Promise<void>
	{
		if (element.isFolder) return;

		await element.updateStatus();
		await this.refresh(element);
		await settings.update(element);
	}

	/**
	 * Update entry name
	 * @param offset Line to update
	 */
	async renameItem(element: Dependency): Promise<void>
	{
		if (element.isFolder) return;

		const data = await settings.get();
		const existingNames = data.find(i => i.folder.uri === element.folder.uri)?.hiddenFiles.map(f => f.path);

		vscode.window.showInputBox({
			validateInput: (item) => existingNames?.includes(item) ? 'This path already exists' : '',
			placeHolder: `New name (current: ${element.label})`
		}).then(async newName => {
			if (newName) {
				newName = newName.trim();
				const oldName = element.label;
				await element.updateName(newName);
				// await this.refresh(element);

				if (vscode.workspace.workspaceFolders?.length === 1) {
					this.parents[0].hiddenFiles = this.parents[0].hiddenFiles?.filter(i => i.path !== oldName);
					this.parents[0].hiddenFiles?.push({ path: newName, hidden: element.isHidden });
					await this.refresh();
				} else {
					if (!element.parent) return;
					element.parent.hiddenFiles = element.parent.hiddenFiles?.filter(i => i.path !== oldName);
					element.parent.hiddenFiles?.push({ path: newName, hidden: element.isHidden });
					await this.refresh(element.parent);
				}		

				await settings.updateName(element, oldName);
			}
		});
	}

	/**
	 * Remove entry
	 * @param offset Line to remove
	 */
	async removeItem(element: Dependency): Promise<void>
	{
		if (element.isFolder) return;

		if (!element.parent || !element.parent.hiddenFiles) {
			return;
		}

		if (vscode.workspace.workspaceFolders?.length === 1) {
			this.parents[0].hiddenFiles = this.parents[0].hiddenFiles?.filter(i => i.path !== element.label);
			await this.refresh();
		} else {
			element.parent.hiddenFiles = element.parent.hiddenFiles.filter(i => i.path !== element.label);
			await this.refresh(element.parent);
		}

		await settings.remove(element);
	}

	/**
	 * Add entry
	 */
	async addItem(element: Dependency): Promise<void>
	{
		if (typeof element === "undefined" && vscode.workspace.workspaceFolders?.length !== 1) return;
		if (typeof element === "undefined") element = this.parents[0];

		if (!element.isFolder) return;

		const data = await settings.get();
		const existingNames = data.find(i => i.folder.uri === element.folder.uri)?.hiddenFiles.map(f => f.path);

		vscode.window.showInputBox({
			validateInput: (item) => existingNames?.includes(item) ? 'This path already exists' : '',
			placeHolder: 'Enter file to exclude'
		}).then(async newName => {
			if (newName) {
				newName = newName.trim();
				await this.addItemToDependency(element, newName);
			}
		});
	}

	async addItemToDependency(element: Dependency, key: string, value: boolean = true): Promise<void>
	{
		if (vscode.workspace.workspaceFolders?.length === 1) {
			this.parents[0].hiddenFiles?.push({ path: key, hidden: value });
			await this.refresh();
		} else {
			element.hiddenFiles?.push({ path: key, hidden: value });
			await this.refresh(element);
		}

		await settings.add(element, key, value);
	}

	////// EXPLORER ACTIONS /////////////////////////////////////////////////////////////////////

	async addToHiddenFiles(element: any): Promise<void>
	{
		if (element.scheme !== "file") return;

		const fileFolder = vscode.workspace.getWorkspaceFolder(vscode.Uri.file(element.path));
		if (typeof fileFolder === "undefined") return;
		
		const dependency = this.parents.find(p => p.folder.uri === fileFolder.uri);
		if (typeof dependency === "undefined") return;

		const key = vscode.workspace.asRelativePath(element.path, false);
		this.addItemToDependency(dependency, key);
	}
}
