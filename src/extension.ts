// Copyright (C) 2023 Sebastien Guerri
//
// simple-hide-files is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or any later version.
//
// simple-hide-files is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.

"use strict";

import * as vscode from "vscode";

import { HideFilesProvider } from "./hideFiles";

/**
 * Activate the extension
 * @param context Extension context
 */
export function activate()
{
	// Create provider
	const hideFilesProvider = new HideFilesProvider();
	
	vscode.commands.executeCommand("setContext", "hideFiles.singleton", false);

	// Register provider
	vscode.window.registerTreeDataProvider("hideFiles", hideFilesProvider);
	
	// Register view commands
	vscode.commands.registerCommand("hideFiles.refresh", () => hideFilesProvider.reloadAndRefresh());
	vscode.commands.registerCommand("hideFiles.updateItem", element => hideFilesProvider.updateItem(element));
	vscode.commands.registerCommand("hideFiles.renameItem", element => hideFilesProvider.renameItem(element));
	vscode.commands.registerCommand("hideFiles.removeItem", element => hideFilesProvider.removeItem(element));
	vscode.commands.registerCommand("hideFiles.addItem", element => hideFilesProvider.addItem(element));

	// Register explorer command
	vscode.commands.registerCommand("hideFiles.addToHiddenFiles", element => hideFilesProvider.addToHiddenFiles(element));
}
