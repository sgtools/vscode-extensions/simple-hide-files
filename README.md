# Welcome to simple-hide-files

[![](https://flat.badgen.net/gitlab/release/sgtools%2Fvscode-extensions/simple-hide-files?label=Current%20release)](https://gitlab.com/sgtools/vscode-extensions/simple-hide-files/-/releases)
[![](https://flat.badgen.net/vs-marketplace/v/sguerri.simple-hide-files)](https://marketplace.visualstudio.com/items?itemName=sguerri.simple-hide-files)
[![](https://flat.badgen.net/open-vsx/version/sguerri/simple-hide-files?label=Open%20VSX)](https://open-vsx.org/extension/sguerri/simple-hide-files)
[![](https://flat.badgen.net/gitlab/license/sgtools%2Fvscode-extensions/simple-hide-files?label=License)](https://gitlab.com/sgtools/vscode-extensions/simple-hide-files/-/blob/main/LICENSE)
[![](https://flat.badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](#)

> Simple Hide Files from explorer for vscodium and vscode

This is an extension for [vscodium](https://vscodium.com/) and [vscode](https://code.visualstudio.com/).

Creates a sidebar pane to handle hidden files as per `files.exclude` entry in `.vscode/settings.json` file.

**Main features**
* Displays the content of `files.exclude` field
* Hide file / folder from explorer
* Add / Remove / Rename items
* Single / Multiple folders in workspace
* Auto-update on `.vscode/settings.json` and workspace changes

## Installation

The extension is available in [Open VSX Registry](https://open-vsx.org/extension/sguerri/simple-hide-files) as well as [VS Code Marketplace](https://marketplace.visualstudio.com/items?itemName=sguerri.simple-hide-files).

For a manual install, download latest vsix file. Go to `Extensions > Install from VSIX...` and select downloaded file.

## Usage

![](./resources/demo.gif)

## Dependencies

- [vscode-icons](https://github.com/microsoft/vscode-icons)

## Author

Sébastien Guerri - [gitlab page](https://gitlab.com/sguerri)

## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/vscode-extensions/simple-hide-files/-/issues). You can also contact me.

## License

Copyright (C) 2023 Sebastien Guerri

simple-hide-files is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

simple-hide-files is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.
