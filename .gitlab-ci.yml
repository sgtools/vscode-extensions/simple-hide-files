# Copyright (C) 2023 Sebastien Guerri
#
# simple-hide-files is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or any later version.
#
# simple-hide-files is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with simple-hide-files. If not, see <https://www.gnu.org/licenses/>.

variables:
  PACKAGE_USER: "sguerri"
  PACKAGE_NAME: "simple-hide-files"
  PACKAGE_VERSION: "0.2.6"
  VSIX_FILE: "$PACKAGE_NAME-$PACKAGE_VERSION.vsix"
  GENERIC_REGISTRY_URL:  $CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$PACKAGE_NAME/$PACKAGE_VERSION

stages:
  - build
  - upload
  - release

building:
  stage: build
  image: node:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Build starting ..."
    - npm ci
    - echo "Packaging ..."
    - npm run package
    - echo "Publishing to VSCode Marketplace ..."
    - npm run deploy
    - echo "Publishing to Open VSX ..."
    - npm run ovsx -- --pat $OPENVSX_TOKEN publish "$VSIX_FILE"
    - echo "Done!"
  artifacts:
    paths:
      - "$VSIX_FILE"

uploading:
  stage: upload
  needs: [building]
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $VSIX_FILE "$GENERIC_REGISTRY_URL/$VSIX_FILE"

releasing:
  stage: release
  needs: [uploading]
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "Releasing ..."
  release:
    name: "$CI_COMMIT_TAG"
    tag_name: "$CI_COMMIT_TAG"
    description: "$CI_COMMIT_TAG"
    assets:
      links:
        - name: "$VSIX_FILE"
          url: "$GENERIC_REGISTRY_URL/$VSIX_FILE"
